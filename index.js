// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require("express");

// Import mongooseJS
const mongoose = require("mongoose");

// Khởi tạo app express
const app = express();

// Khai báo middleware đọc json
app.use(express.json());

// Khai báo middleware đọc dữ liệu UTF-8
app.use(express.urlencoded({
 extended: true
}));

// Khai báo cổng của project
const port = 8000;

//Import router
const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");
const APIUserRouter = require("./app/routes/APIUserRouter");

mongoose.connect("mongodb://localhost:27017/CRUD_Pizza365", (err) => {
    if(err) {
        throw err;
    }

    console.log("Connect MongoDB successfully!");
});

app.use((request, response, next) => {
 console.log("Time", new Date());
 next();
},
(request, response, next) => {
 console.log("Request method: ", request.method);
 next();
}
)

app.get("/", (request, response) => {
 let today = new Date();

 response.status(200).json({
     message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
 })
});

app.use(drinkRouter);
app.use(voucherRouter);
app.use(userRouter);
app.use(orderRouter);
app.use("/", APIUserRouter);

// Chạy app express
app.listen(port, () => {
 console.log("App listening on port (Ứng dụng đang chạy trên cổng) " + port);
});