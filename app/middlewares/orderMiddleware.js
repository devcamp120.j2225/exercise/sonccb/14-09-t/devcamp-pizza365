const printOrderURLMiddleware = (request, response, next) => {
    console.log("User URL: ", request.url);
    next();
}

module.exports = {
    printOrderURLMiddleware : printOrderURLMiddleware
}