// Import course model vào controller
const userModel = require("../models/userModel");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const getAllUserWithLimit = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let limitUser = request.query.limitUser;
    let condition = "";
    //B2: Validate dữ liệu
    if (!limitUser) {
        condition = 0;
    } else {
        condition = limitUser;
    }
    console.log(condition);
    //B3: Thao tác với cơ sở dữ liệu

    userModel.find({})
    .limit(condition)
    .exec((error,data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    }
    )
}

const getAllUserWithSkip = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let skipUser = request.query.skipUser;
    let condition = "";
    //B2: Validate dữ liệu
    if (!skipUser) {
        condition = 0;
    } else {
        condition = skipUser;
    }
    console.log(condition);
    //B3: Thao tác với cơ sở dữ liệu

    userModel.find({})
    .skip(condition)
    .exec((error,data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    }
    )
}

const getAllUserWithSortIncreament = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu

    userModel.find({})
    .sort({fullName:"asc"})
    .exec((error,data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    }
    )
}

const getAllUserWithSkipLimit = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let limitUser = request.query.limitUser;
    let skipUser = request.query.skipUser;

    //B2: Validate dữ liệu
    if (!limitUser) {
        limitUser = 0;
    }

    if (!skipUser) {
        skipUser = 0;
    }
    //B3: Thao tác với cơ sở dữ liệu

    userModel.find({})
    .skip(skipUser)
    .limit(limitUser)
    .exec((error,data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    }
    )
}

const getAllUserWithSortSkipLimit = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let limitUser = request.query.limitUser;
    let skipUser = request.query.skipUser;

    //B2: Validate dữ liệu
    if (!limitUser) {
        limitUser = 0;
    }

    if (!skipUser) {
        skipUser = 0;
    }
    //B3: Thao tác với cơ sở dữ liệu

    userModel.find({})
    .sort({fullName:"asc"})
    .skip(skipUser)
    .limit(limitUser)
    .exec((error,data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    }
    )
}

// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
    getAllUserWithLimit: getAllUserWithLimit,
    getAllUserWithSkip: getAllUserWithSkip,
    getAllUserWithSortIncreament: getAllUserWithSortIncreament,
    getAllUserWithSkipLimit: getAllUserWithSkipLimit,
    getAllUserWithSortSkipLimit:getAllUserWithSortSkipLimit

}