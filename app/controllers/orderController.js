// Import course model vào controller
const orderModel = require("../models/orderModel");
const userModel = require("../models/userModel");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createOrder = (request, response) => {
    // B1: Thu thập dữ liệu
    let userId = request.params.userId;
    let bodyRequest = request.body;

    // B2: Kiểm tra dữ liệu
    if(!bodyRequest.orderCode) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order Code is required"
        })
    }
    if(!bodyRequest.pizzaSize) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Pizza Size is required"
        })
    }
    if(!bodyRequest.pizzaType) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Pizza Type is required"
        })
    }
    if(!bodyRequest.status) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Status is required"
        })
    }

   

    // B3: Thao tác với cơ sở dữ liệu
    let createOrder = {
        _id: mongoose.Types.ObjectId(),
        orderCode: bodyRequest.orderCode,
        pizzaSize: bodyRequest.pizzaSize, 
        pizzaType: bodyRequest.pizzaType,
        status: bodyRequest.status
    }

    orderModel.create(createOrder, (error, data) => {
     if(error) {
         return response.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
      userModel.findByIdAndUpdate(userId, 
             {
                $push: { orders: data._id } 
             },
             (err, updatedCourse) => {
                 if(err) {
                     return response.status(500).json({
                         status: "Error 500: Internal server error",
                         message: err.message
                     })
                 } else {
                     return response.status(201).json({
                         status: "Create Order Success",
                         data: data
                     })
                 }
             }
         )
     }
 })
    
};

const getAllOrder = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
      return response.status(400).json({
       status:"Error 400: Bad Request",
       message:"Order ID is invalid"
      })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId)
    .populate("orders")
    .exec((error,data) => {
     if (error) {
      return response.status(500).json({
       status: "Error 500: Internal server error",
       message: error.message
      })
     } else {
        return response.status(200).json({
         status: "Get Order success",
         data: data.orders
     })
     }
    })
}

const getOrderById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findById(orderId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get Order success",
                data: data
            })
        }
    })
}

const updateOrderById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let orderUpdate = {
     orderCode: bodyRequest.orderCode,
     pizzaSize: bodyRequest.pizzaSize, 
     pizzaType: bodyRequest.pizzaType,
     status: bodyRequest.status
    }

    orderModel.findByIdAndUpdate(orderId, orderUpdate, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update Order success",
                data: data
            })
        }
    })
}

const deleteOrderById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let orderId = request.params.orderId;
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Order ID is not valid"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    orderModel.findByIdAndDelete(orderId, (error) => {
     if(error) {
         return response.status(500).json({
             status: "Error 500: Internal server error",
             message: error.message
         })
     } else {
         // Sau khi xóa xong 1 order khỏi collection cần xóa thêm orderID trong user đang chứa nó
         userModel.findByIdAndUpdate(userId, 
             {
                 $pull: { orders: orderId } 
             },
             (err, updatedUser) => {
                 if(err) {
                     return response.status(500).json({
                         status: "Error 500: Internal server error",
                         message: err.message
                     })
                 } else {
                     return response.status(204).json({
                         status: "Success: Delete Order success"
                     })
                 }
             })
     }
 })
}


// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
 createOrder: createOrder,
 getAllOrder: getAllOrder,
 getOrderById: getOrderById,
 updateOrderById: updateOrderById,
 deleteOrderById: deleteOrderById
}