// Import course model vào controller
const userModel = require("../models/userModel");

// Khai báo thư viện mongoose 
const mongoose = require("mongoose");

const createUser = (request, response) => {
    // B1: Thu thập dữ liệu
    let bodyRequest = request.body;

    // B2: Kiểm tra dữ liệu
    if(!bodyRequest.fullName) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Fullname is required"
        })
    }
    if(!bodyRequest.email) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Email is required"
        })
    }
    if(!bodyRequest.address) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "Address is required"
        })
    }
    if(!bodyRequest.phone) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "phone is required"
        })
    }

   

    // B3: Thao tác với cơ sở dữ liệu
    let createUser = {
        _id: mongoose.Types.ObjectId(),
        fullName: bodyRequest.fullName,
        email: bodyRequest.email, 
        address: bodyRequest.address,
        phone: bodyRequest.phone
    }

    userModel.create(createUser, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success: User created",
                data: data
            })
        }
    })
}

const getAllUser = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    //B2: Validate dữ liệu
    //B3: Thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    })
}

const getUserById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findById(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Get user success",
                data: data
            })
        }
    })
}

const updateUserById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    let bodyRequest = request.body;

    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    let userUpdate = {
     fullName: bodyRequest.fullName,
     email: bodyRequest.email, 
     address: bodyRequest.address,
     phone: bodyRequest.phone
    }

    userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Success: Update user success",
                data: data
            })
        }
    })
}

const deleteUserById = (request, response) => {
    //B1: Chuẩn bị dữ liệu
    let userId = request.params.userId;
    //B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)) {
        return response.status(400).json({
            status: "Error 400: Bad Request",
            message: "User ID is not valid"
        })
    }
    //B3: Thao tác với cơ sở dữ liệu
    userModel.findByIdAndDelete(userId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Error 500: Internal server error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Success: Delete User success"
            })
        }
    })
}

// Export controller thành 1 module là 1 object gồm các hàm trong controller
module.exports = {
 createUser: createUser,
 getAllUser: getAllUser,
 getUserById: getUserById,
 updateUserById: updateUserById,
 deleteUserById: deleteUserById
}