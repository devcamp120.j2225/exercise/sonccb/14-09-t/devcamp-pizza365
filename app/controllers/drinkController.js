// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Course Model
const drinkModel = require("../models/drinkModel");

// Create drink
const createDrink = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let body = req.body;

 // B2: Validate dữ liệu
 if(!body.maNuocUong) {
     return res.status(400).json({
         message: "Drink code is required!"
     })
 }

 if(!body.tenNuocUong) {
  return res.status(400).json({
      message: "Drink name is required!"
  })
}

if(body.donGia !== undefined && (!Number.isInteger(body.donGia) || body.donGia < 0)) {
 return res.status(400).json({
     message: "Price is invalid!"
 })
}

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let newDrinkData = {
     _id: mongoose.Types.ObjectId(),
     maNuocUong: body.maNuocUong,
     tenNuocUong: body.tenNuocUong,
     donGia: body.donGia
 }

 drinkModel.create(newDrinkData, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: "Create successfully",
         newDrink: data
     })
 })
}

// Get all course 
const getAllDrink = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 // B2: Validate dữ liệu
 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 drinkModel.find((error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Get all drink successfully",
         drinks: data
     })
 })
}

// Get course by id
const getDrinkById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let drinkId = req.params.drinkId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(drinkId)) {
     return res.status(400).json({
         message: "Drink ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 drinkModel.findById(drinkId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: "Get drinks successfully",
         drink: data
     })
 })
}

// Update course by id
const updateDrinkById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let drinkId = req.params.drinkId;
 let body = req.body;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(drinkId)) {
     return res.status(400).json({
         message: "Drink ID is invalid!"
     })
 }

 // Bóc tách trường hợp undefied
 if(body.maNuocUong !== undefined && body.maNuocUong == "") {
     return res.status(400).json({
         message: "Drink code is required!"
     })
 }

 if(body.tenNuocUong !== undefined && body.tenNuocUong == "") {
  return res.status(400).json({
      message: "Drink name is required!"
  })
}

 if(body.donGia !== undefined && (!Number.isInteger(body.donGia) || body.donGia < 0)) {
     return res.status(400).json({
         message: "Price is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let drinkUpdate = {
   //_id: mongoose.Types.ObjectId(),
   maNuocUong: body.maNuocUong,
   tenNuocUong: body.tenNuocUong,
   donGia: body.donGia
 };

 drinkModel.findByIdAndUpdate(drinkId, drinkUpdate, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Update drink successfully",
         updatedDrink: data
     })
 })
}

// Delete course by id
const deleteDrinkById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let drinkId = req.params.drinkId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(drinkId)) {
     return res.status(400).json({
         message: "Drink ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 drinkModel.findByIdAndDelete(drinkId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(204).json({
         message: "Delete drink successfully"
     })
 })
}

// Export Course controller thành 1 module
module.exports = {
    createDrink,
    getAllDrink,
    getDrinkById,
    updateDrinkById,
    deleteDrinkById
}
