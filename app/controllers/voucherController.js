// Import thư viện mongoose
const mongoose = require("mongoose");

// Import Course Model
const voucherModel = require("../models/voucherModel");

// Create drink
const createVoucher = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let body = req.body;

 // B2: Validate dữ liệu
 if(!body.maVoucher) {
     return res.status(400).json({
         message: "maVoucher code is required!"
     })
 }
 if(!isNaN(body.maVoucher)) {
  return res.status(400).json({
      message: "maVoucher code is String!"
  })
}

 if(!body.phanTramGiamGia) {
  return res.status(400).json({
      message: "Percent discount is required!"
  })
}

if(body.phanTramGiamGia !== undefined && (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0 || body.phanTramGiamGia > 100)) {
 return res.status(400).json({
     message: "Percent discount is invalid!"
 })
}

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let newVoucherData = {
     _id: mongoose.Types.ObjectId(),
     maVoucher: body.maVoucher,
     phanTramGiamGia: body.phanTramGiamGia,
     ghiChu: body.ghiChu
 }

 voucherModel.create(newVoucherData, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: "Create successfully",
         newVoucher: data
     })
 })
}

// Get all course 
const getAllVoucher = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 // B2: Validate dữ liệu
 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 voucherModel.find((error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Get all voucher successfully",
         courses: data
     })
 })
}

// Get course by id
const getVoucherById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let voucherId = req.params.voucherId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(voucherId)) {
     return res.status(400).json({
         message: "Voucher ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 voucherModel.findById(voucherId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(201).json({
         message: "Get drinks successfully",
         voucher: data
     })
 })
}

// Update course by id
const updateVoucherById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let voucherId = req.params.voucherId;
 let body = req.body;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(voucherId)) {
     return res.status(400).json({
         message: "Voucher ID is invalid!"
     })
 }

 // Bóc tách trường hợp undefied
 if(body.maVoucher !== undefined && body.maVoucher == "") {
     return res.status(400).json({
         message: "Voucher code is required!"
     })
 }

 if(body.phanTramGiamGia !== undefined && body.phanTramGiamGia == "") {
  return res.status(400).json({
      message: "Percent discount is required!"
  })
}

if(body.phanTramGiamGia !== undefined && (!Number.isInteger(body.phanTramGiamGia) || body.phanTramGiamGia < 0 || body.phanTramGiamGia > 100)) {
 return res.status(400).json({
     message: "Percent discount is invalid!"
 })
}

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 let voucherUpdate = {
   //_id: mongoose.Types.ObjectId(),
   maVoucher: body.maVoucher,
   phanTramGiamGia: body.phanTramGiamGia,
   ghiChu: body.ghiChu
 };

 voucherModel.findByIdAndUpdate(voucherId, voucherUpdate, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(200).json({
         message: "Update voucher successfully",
         updatedVoucher: data
     })
 })
}

// Delete course by id
const deleteVoucherById = (req, res) => {
 // B1: Thu thập dữ liệu từ req
 let voucherId = req.params.voucherId;

 // B2: Validate dữ liệu
 if(!mongoose.Types.ObjectId.isValid(voucherId)) {
     return res.status(400).json({
         message: "Voucher ID is invalid!"
     })
 }

 // B3: Gọi model thực hiện các thao tác nghiệp vụ
 voucherModel.findByIdAndDelete(voucherId, (error, data) => {
     if(error) {
         return res.status(500).json({
             message: error.message
         })
     }

     return res.status(204).json({
         message: "Delete drink successfully"
     })
 })
}

// Export Course controller thành 1 module
module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}