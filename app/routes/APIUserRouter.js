// Import bộ thư viện express
const express = require('express'); 

// Import Course Middleware
const { printUserURLMiddleware } = require("../middlewares/userMiddleware");

// Import Course Controller
const {
 getAllUserWithLimit, 
 getAllUserWithSkip,
 getAllUserWithSortIncreament,
 getAllUserWithSkipLimit,
 getAllUserWithSortSkipLimit
  
 } = require("../controllers/APIuserController");

const router = express.Router();

router.get("/limit-users",printUserURLMiddleware, getAllUserWithLimit);

router.get("/skip-users",printUserURLMiddleware, getAllUserWithSkip);

router.get("/sort-users",printUserURLMiddleware, getAllUserWithSortIncreament);

router.get("/skip-limit-users",printUserURLMiddleware, getAllUserWithSkipLimit);

router.get("/sort-skip-limit-users",printUserURLMiddleware, getAllUserWithSortSkipLimit);


// Export dữ liệu thành 1 module
module.exports = router;