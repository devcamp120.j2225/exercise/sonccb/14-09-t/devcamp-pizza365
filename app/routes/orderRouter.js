// Import bộ thư viện express
const express = require('express'); 

// Import Course Middleware
const { printOrderURLMiddleware } = require("../middlewares/orderMiddleware");

// Import Course Controller
const { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById } = require("../controllers/orderController");

const router = express.Router();

router.get("/users/:userId/orders",printOrderURLMiddleware, getAllOrder);

router.post("/users/:userId/orders",printOrderURLMiddleware, createOrder);

router.get("/orders/:orderId",printOrderURLMiddleware, getOrderById);

router.put("/orders/:orderId",printOrderURLMiddleware, updateOrderById);

router.delete("/users/:userId/orders/:orderId",printOrderURLMiddleware, deleteOrderById);

// Export dữ liệu thành 1 module
module.exports = router;