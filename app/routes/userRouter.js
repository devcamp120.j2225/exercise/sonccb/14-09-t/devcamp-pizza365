// Import bộ thư viện express
const express = require('express'); 

// Import Course Middleware
const { printUserURLMiddleware } = require("../middlewares/userMiddleware");

// Import Course Controller
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require("../controllers/userController");

const router = express.Router();

router.get("/users",printUserURLMiddleware, getAllUser);

router.post("/users",printUserURLMiddleware, createUser);

router.get("/users/:userId",printUserURLMiddleware, getUserById);

router.put("/users/:userId",printUserURLMiddleware, updateUserById);

router.delete("/users/:userId",printUserURLMiddleware, deleteUserById);

// Export dữ liệu thành 1 module
module.exports = router;