// B1: Import mongooseJS
const mongoose = require("mongoose");

// B2: Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const orderSchema = new Schema({
  _id:{
   type:mongoose.Types.ObjectId
  },
  orderCode:{
   type:String,
   unique:true,
   default:"123456"
  },
  pizzaSize:{
   type:String,
   required:true,
  },
  pizzaType:{
   type:String,
   required:true
  },
  voucher:[
   {
    type:mongoose.Types.ObjectId,
    ref:"Voucher"
   }
  ],
  drink:[
   {
    type:mongoose.Types.ObjectId,
    ref:"Drink"
   }
  ],
  status:{
   type:String,
   required:true
  },
  
},{
 timestamps:true
});

// B4: Export ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("Order", orderSchema);