// B1: Import mongooseJS
const mongoose = require("mongoose");

// B2: Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const userSchema = new Schema({
  _id:{
   type:mongoose.Types.ObjectId
  },
  fullName:{
   type:String,
   required:true
  },
  email:{
   type:String,
   required:true,
   unique:true
  },
  address:{
   type:String,
   required:true
  },
  phone:{
   type:String,
   required:true,
   unique:true
  },
  orders:[
   {
    type:mongoose.Types.ObjectId,
    ref:"Order"
   }
  ]
  
},{
 timestamps:true
});

// B4: Export ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("User", userSchema);