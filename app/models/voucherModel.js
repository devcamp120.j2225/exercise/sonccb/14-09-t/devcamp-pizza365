//import thư viện mongoose
const mongoose = require("mongoose");

//Khai báo class Schema của mongoose
const Schema = mongoose.Schema;

//Khai báo voucher schema
const voucherSchema = new Schema({
 // _id: {
 //  type: Schema.Types.ObjectId,
 //  unique:true
 // },
 maVoucher:{
  type: String,
  unique:true,
  required:true
 },
 phanTramGiamGia:{
  type: Number,
  required:true
 },
 ghiChu:{
  type: String,
  required:false
 },
//  ngayTao:{
//   type: Date,
//   default:Date.now()
//  },
//  ngayCapNhat:{
//     type: Date,
//     default:Date.now()
//    },
},{
 timestamps:true
});

module.exports = mongoose.model("vouchers", voucherSchema);